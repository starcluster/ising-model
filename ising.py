import numpy as np
import random

import sys
import sdl2
import sdl2.ext

BLACK = sdl2.ext.Color(0, 0, 0)
WHITE = sdl2.ext.Color(255, 255, 255)

def init(N):
    grid = np.ones((N,N))
    for i in range(N):
        for j in range(N):
            grid[i,j] = random.choice([-1,1])
    return grid

def metropolis(grid, N, beta, J):
    m = random.randint(0,N-1)
    n = random.randint(0,N-1)
    s = -grid[m,n]
    dE = -J*2*s*(grid[(m-1)%N,n] + grid[m,(n-1)%N] + grid[(m+1)%N,n] + grid[m,(n+1)%N])
    if dE < 0:
        grid[m,n] = s
        return grid
    else:
        e = np.exp(-beta*dE)
        if random.random() < e:
            grid[m, n] = s
        return grid

def computeE(grid, N, J):
    E = 0
    for m in range(N):
        for n in range(N):
            dE = -J*2*grid[m,n]*(grid[(m-1)%N,n] + grid[m,(n-1)%N] + grid[(m+1)%N,n] + grid[m,(n+1)%N])
            E += dE
    return -E/(8*N)

def run(N,f,J):
    sdl2.ext.init()
    window = sdl2.ext.Window("Modele d'Ising", size=(N*f, N*f))
    ws = window.get_surface()
    sdl2.ext.fill(ws, BLACK)
    window.show()
    running = True

    g = init(N)
    while running:
        for i in range(1000):
            g = metropolis(g, N, 15, J) # 0.1 3 10
        pixelview = sdl2.ext.PixelView(ws)
        for i in range(0, N*f, f):
            for j in range(0, N*f, f):
                if g[int(i/f),int(j/f)] == 1:
                    COLOR = WHITE
                else:
                    COLOR = BLACK
                for k in range(f):
                    for p in range(f):
                        pixelview[i+k][j+p] = COLOR

        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
                break
        print(computeE(g,N,J))
        window.refresh()
    return 0

N = 100
f = 2
J = 1

if __name__ == "__main__":
    sys.exit(run(N, f, J))


