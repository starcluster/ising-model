#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <cmath>

#include <SDL2/SDL.h>


using namespace std;

int mod(int a, int b)
{
    int r = a % b;
    return r < 0 ? r + b : r;
}

void init(vector<vector<int > > & a, int N){
  for(int i = 0; i < N; i++)
      for(int j = 0; j < N; j++)
	a[i][j] = (rand()%2)*2-1;
}

void printGrid(vector<vector<int > > & a, int N){
  for(int i = 0; i < N; i++){
    for(int j = 0; j < N; j++){
      if (a[i][j] == 1)
	cout << " " << a[i][j] << " ";
      else cout << a[i][j] << " ";
    }
    cout << endl;
  }
}

void metropolis(vector<vector<int > > & g, int N, double beta, double J, int *indi, int *indj){
  int m = rand()%N;
  int n = rand()%N;
  int s = -g[m][n];
   //  /!\ % in c++ is not really a mod !
  double h = 0;
  double dE = -2*s*J*(g[mod(m-1,N)][n] + g[m][mod(n-1,N)] + g[(m+1)%N][n] + g[m][(n+1)%N]) + h*g[m][n];
  if (dE < 0) g[m][n] = s;
  else {
    double e = exp(-beta*dE);
    if (rand()/(RAND_MAX*1.) < e){
      g[m][n] = s;
    }
  }
  *indi = m;
  *indj = n;
}

double computeE(vector<vector<int> > & g, int N, double J){
  double dE = 0, E = 0;
  for(int m = 0; m < N; m++){
    for(int n = 0; n < N; n++){
      dE = -2*g[m][n]*J*(g[mod(m-1,N)][n] + g[m][mod(n-1,N)] + g[(m+1)%N][n] + g[m][(n+1)%N]);
      E += dE;
    }
  }
  return E;
}
  
int main(){
  int resol = 8;
  srand(time(NULL));
  int N = 1000/resol;
  cout << N << endl;
  int indi = 0, indj = 0;
  vector<vector<int> > a(N);
  for(int i = 0; i < N; i++)
    a[i].resize(N);
  init(a, N);
  
  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    cout << "error initializing SDL: %s\n" << SDL_GetError() <<endl;
  }
  SDL_Event event;
  SDL_Renderer *renderer;
  SDL_Window *window;

  SDL_CreateWindowAndRenderer(resol*N, resol*N, 0, &window, &renderer);
  
  double beta = 0.1;

  while (true) {
    if (SDL_PollEvent(&event) && event.type == SDL_QUIT)
      break;
    if (event.type == SDL_KEYDOWN){
      if (event.key.keysym.sym == SDLK_UP)
	{
	  if (beta-0.1 > 0)
	    beta = beta - 0.1;
	}
      else if (event.key.keysym.sym == SDLK_DOWN) {
	beta = beta +0.1;
      }
      else{
	cout << "This key isn't assignated" << endl;
      }
    }
    SDL_RenderClear(renderer);
    //SDL_UpdateWindowSurface(window);

    for(int i = 0; i < resol*N; i+=resol){
      for(int j = 0; j < resol*N; j+=resol){
	if (a[i/resol][j/resol] == 1)
	  SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	else   SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	for(int k = 0; k < resol; k++)
	  for(int p = 0; p < resol; p++)
	    SDL_RenderDrawPoint(renderer, i+p, j+k);

    }
  }
    SDL_RenderPresent(renderer);
    

    cout << computeE(a, N, 1.) <<endl;;
    cout << beta << endl;
    
    for(int i = 0; i < 100000; i++)
      metropolis(a, N, beta, 1., &indi, &indj);
    //printGrid(a, N);
  }
  return 0;
}
